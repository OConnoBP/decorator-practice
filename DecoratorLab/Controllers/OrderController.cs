﻿using System.Web.Mvc;
using DecoratorLab.Models;
using System.Data.SqlClient;
namespace DecoratorLab.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitOrder(string beverageName, string[] condiments)
        {
            BeverageFactory aBeverageFactory = new BeverageFactory();
            BeverageStore aBeverageStore = new BeverageStore(aBeverageFactory);
            CondimentFactory aCondimentFactory = new CondimentFactory();
            CondimentStore aCondimentStore = new CondimentStore(aCondimentFactory);

            Beverage beverage = aBeverageStore.OrderBeverage(beverageName);
            beverage = aCondimentStore.OrderCondiments(beverage, condiments);
            string condimentString = "";
            int i = 0;
            if(condiments != null)
            {
                foreach (var condiment in condiments)
                {
                    if (i == 0)
                    {
                        condimentString = $"{condimentString}{condiment}";
                    }
                    else
                    {
                        condimentString = $"{condimentString}, {condiment}";
                    }
                    i++;
                }
            }
            

            Order order = new Order()
            {
                Beverage = beverageName,
                Condiments = condimentString,
                Total = beverage.Cost()
            };

            // Unsecure connection and should sanitize/parameterize but won't since it's a personal project.
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV13;Initial Catalog=CoffeeDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            SqlCommand query = new SqlCommand();
            query = conn.CreateCommand();
            query.CommandText = $"INSERT INTO Orders(Beverage, Condiments, Total) VALUES('{order.Beverage}', '{order.Condiments}', '{order.Total}');";
            conn.Open();
            query.ExecuteNonQuery();
            conn.Close();
            ViewBag.Description = beverage.GetDescription();
            ViewBag.Cost = beverage.Cost();
            
            return View();
        }
    }
}