﻿namespace DecoratorLab.Models
{
    public class Order
    {
        public string Beverage { get; set; }
        public string Condiments { get; set; }
        public decimal Total { get; set; }
    }
}