﻿namespace DecoratorLab.Models
{
    public class CondimentFactory
    {
        public virtual Beverage AddCondiments(Beverage aBeverage, string[] condimentNames)
        {
            if(condimentNames != null)
            {
                foreach (var condiment in condimentNames)
                {
                    if (condiment.Equals("Chocolate"))
                    {
                        aBeverage = new Chocolate(aBeverage);
                    }
                    else if (condiment.Equals("Cream"))
                    {
                        aBeverage = new Cream(aBeverage);
                    }
                    else if (condiment.Equals("Ice"))
                    {
                        aBeverage = new Ice(aBeverage);
                    }
                    else if (condiment.Equals("Latte"))
                    {
                        aBeverage = new Latte(aBeverage);
                    }
                    else if (condiment.Equals("Mocha"))
                    {
                        aBeverage = new Mocha(aBeverage);
                    }
                    else if (condiment.Equals("Orange Zest"))
                    {
                        aBeverage = new OrangeZest(aBeverage);
                    }
                    else if (condiment.Equals("Pumpkin Spice"))
                    {
                        aBeverage = new PumpkinSpice(aBeverage);
                    }
                    else if (condiment.Equals("Skim"))
                    {
                        aBeverage = new Skim(aBeverage);
                    }
                    else if (condiment.Equals("Vanilla"))
                    {
                        aBeverage = new Vanilla(aBeverage);
                    }
                }
            }
            
            return aBeverage;
        }
    }
}