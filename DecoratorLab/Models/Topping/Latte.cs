﻿namespace DecoratorLab.Models
{
    public class Latte : CondimentDecorator
    {
        Beverage beverage = null;

        public Latte(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Latte";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 2.00M;
        }
    }
}