﻿namespace DecoratorLab.Models
{
    public class Chocolate : CondimentDecorator
    {
        Beverage beverage = null;

        public Chocolate(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Chocolate";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 0.50M;
        }
    }
}