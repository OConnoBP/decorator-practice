﻿namespace DecoratorLab.Models
{
    public class Mocha : CondimentDecorator
    {
        Beverage beverage = null;

        public Mocha(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Mocha";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 0.75M;
        }
    }
}