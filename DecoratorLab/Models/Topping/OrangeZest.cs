﻿namespace DecoratorLab.Models
{
    public class OrangeZest : CondimentDecorator
    {
        Beverage beverage = null;

        public OrangeZest(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Orange Zest";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 0.50M;
        }
    }
}