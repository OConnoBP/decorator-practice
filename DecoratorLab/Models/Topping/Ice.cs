﻿namespace DecoratorLab.Models
{
    public class Ice : CondimentDecorator
    {
        Beverage beverage = null;

        public Ice(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Ice";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 0.30M;
        }
    }
}