﻿namespace DecoratorLab.Models
{
    public class Vanilla : CondimentDecorator
    {
        Beverage beverage = null;

        public Vanilla(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Vanilla";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 1.50M;
        }
    }
}