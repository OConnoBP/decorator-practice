﻿namespace DecoratorLab.Models
{
    public class CondimentStore
    {
        CondimentFactory condimentFactory = null;

        public CondimentStore(CondimentFactory aCondimentFactory)
        {
            this.condimentFactory = aCondimentFactory;
        }

        public Beverage OrderCondiments(Beverage aBeverage, string[] condiments)
        {
            return this.condimentFactory.AddCondiments(aBeverage, condiments);
        }
    }
}