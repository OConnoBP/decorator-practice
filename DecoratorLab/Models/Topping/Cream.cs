﻿namespace DecoratorLab.Models
{
    public class Cream : CondimentDecorator
    {
        Beverage beverage = null;

        public Cream(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Cream";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 1.00M;
        }
    }
}