﻿namespace DecoratorLab.Models
{
    public class Skim : CondimentDecorator
    {
        Beverage beverage = null;

        public Skim(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Skim";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 1.00M;
        }
    }
}