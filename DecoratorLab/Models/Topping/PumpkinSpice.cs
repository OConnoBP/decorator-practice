﻿namespace DecoratorLab.Models
{
    public class PumpkinSpice : CondimentDecorator
    {
        Beverage beverage = null;

        public PumpkinSpice(Beverage aBeverage)
        {
            this.beverage = aBeverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Pumpkin Spice";
        }

        public override decimal Cost()
        {
            return this.beverage.Cost() + 1.00M;
        }
    }
}