﻿namespace DecoratorLab.Models
{
    public class Decaf : Beverage
    {
        public Decaf()
        {
            this.description = "Decaf: No caffine";
        }

        public override decimal Cost()
        {
            return 1.00M;
        }
    }
}