﻿namespace DecoratorLab.Models
{
    public class ItalianRoast : Beverage
    {
        public ItalianRoast()
        {
            this.description = "Italian Roast: bittersweet to burned or charred tastes";
        }

        public override decimal Cost()
        {
            return 1.00M;
        }
    }
}