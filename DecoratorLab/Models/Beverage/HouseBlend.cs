﻿namespace DecoratorLab.Models
{
    public class HouseBlend : Beverage
    {
        public HouseBlend()
        {
            this.description = "House Blend: Our own special brew";
        }

        public override decimal Cost()
        {
            return 1.00M;
        }
    }
}