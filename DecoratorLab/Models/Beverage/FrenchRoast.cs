﻿namespace DecoratorLab.Models
{
    public class FrenchRoast : Beverage
    {
        public FrenchRoast()
        {
            this.description = "French Roast: Bittersweet tones";
        }

        public override decimal Cost()
        {
            return 1.00M;
        }
    }
}