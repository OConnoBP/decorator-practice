﻿namespace DecoratorLab.Models
{
    public class BeverageFactory
    {
        public virtual Beverage CreateBeverage(string beverageName)
        {
            Beverage beverage = null;

            if (beverageName == "Decaf")
            {
                beverage = new Decaf();
            }
            else if (beverageName == "French Roast")
            {
                beverage = new FrenchRoast();
            }
            else if (beverageName == "House Blend")
            {
                beverage = new HouseBlend();
            }
            else if (beverageName == "Italian Roast")
            {
                beverage = new ItalianRoast();
            }
            else if (beverageName == "Vienna Roast")
            {
                beverage = new ViennaRoast();
            }

            return beverage;
        }
    }
}