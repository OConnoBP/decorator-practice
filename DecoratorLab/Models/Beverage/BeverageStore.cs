﻿namespace DecoratorLab.Models
{
    public class BeverageStore
    {
        BeverageFactory beverageFactory = null;

        public BeverageStore(BeverageFactory aBeverageFactory)
        {
            this.beverageFactory = aBeverageFactory;
        }

        public Beverage OrderBeverage(string beverageName)
        {
            return beverageFactory.CreateBeverage(beverageName);
        }
    }
}