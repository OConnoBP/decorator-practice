﻿namespace DecoratorLab.Models
{
    public abstract class Beverage
    {
        public string description;
        public virtual string GetDescription()
        {
            return this.description;
        }

        public abstract decimal Cost();
    }
}