﻿namespace DecoratorLab.Models
{
    public class ViennaRoast : Beverage
    {
        public ViennaRoast()
        {
            this.description = "Vienna Roast: a milder roast than French Roast";
        }

        public override decimal Cost()
        {
            return 1.20M;
        }
    }
}